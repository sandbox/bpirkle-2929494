Alters the commerce_authnet settings form to allow overriding the urls used
to communicate with Authorize.net.  After enabling, you'll see two new text
fields at the bottom of the Authorize.net AIM payment method settings.
Use them to adjust the urls to suit your needs.

Strictly speaking, this module is unnecessary.  The urls can already be set
via Drupal variables using Drush.  However, it is sometimes convenient to be
able to set them via the admin ui.

It seemed like there was bound to already be a module that did this, but I
didn't find it.
